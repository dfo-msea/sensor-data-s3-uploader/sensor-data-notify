# AWS Lambda function to generate presigned URL and email to users.

__Main author:__  Cole Fields  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel:


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Generate a presigned URL for a processed csv file within an AWS S3 bucket. Email the URL to a list of recipients.


## Summary
Simple script that is triggered by object creation in an S3 bucket. This overall purpose of the parent project is to perform quality control on oceanographic sea surface temperature data and create a processed csv file with flags based on temperature ranges in the data. A netCDF file is also created, with associated metadata. The csv and netCDF files are zipped together and uploaded to another bucket and a link is emailed to users for downloading the data.


## Status
In-development.


## Contents
One of three repositories within the `sensor-data` parent project. This repo contains a standalone file that is triggered when a processed csv file is uploaded to an S3 bucket (processed, not the source data bucket).


## Methods
This project is built with the following services:
- [Amazon S3](https://aws.amazon.com/s3/)
- [Amazon Lambda](https://aws.amazon.com/lambda/)

__Steps__
1. Add trigger on processed bucket.
2. Create presigned URL.
3. Email that URL to users.

See the WIKI for more details.

## Requirements
- AWS account
- Python3 virtual env with boto3 


## Caveats
* No errors provided to user if processing script fails. Will need to check the CloudWatch logs.


## Acknowledgements
* Hakai Institute (https://hakai.org/)


## References
* https://boto3.amazonaws.com/v1/documentation/api/latest/guide/s3-presigned-urls.html#presigned-urls
* https://docs.aws.amazon.com/ses/latest/dg/send-an-email-using-sdk-programmatically.html
