""" Emails list of recipients with a presigned URL to download processed file 
    from AWS S3 bucket. Link will expire in 5 days. 
"""

import os
import boto3
from botocore.exceptions import ClientError
import json
import logging
import urllib.parse

s3_client = boto3.client("s3")
s3_resource = boto3.resource("s3")
cognito = boto3.client('cognito-idp')
ses_client = boto3.client("ses")

# Configure logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# GLOBALS
RECIPIENT_LIST = ["DFO.PACScienceMSEAData-DonneesEASMSciencePAC.MPO@dfo-mpo.gc.ca", 
          "Katherine.Charles@dfo-mpo.gc.ca",
          "Cole.Fields@dfo-mpo.gc.ca"]
SENDER = "Sensor Data Uploader <DFO.PACScienceMSEAData-DonneesEASMSciencePAC.MPO@dfo-mpo.gc.ca>"
REGION = os.environ["AWS_REGION"]


def get_bucket(event):
    """ Return bucket name as string from AWS S3 bucket object. """
    try:
        logging.info("Getting AWS bucket reference.")
        bucket = event["Records"][0]["s3"]["bucket"]["name"]
        logging.info(f"Bucket name: {bucket}")
        return bucket
    except s3_resource.meta.client.exceptions.NoSuchBucket as e:
        logging.error(e)
        raise e


def get_key(event):
    """ Return key as string from AWS S3 key (file) object. """
    try:
        logging.info("Getting AWS key (file) reference.")
        key = urllib.parse.unquote_plus(event["Records"][0]["s3"]["object"]["key"])
        logging.info(f"Key: {key}")
        return key
    except s3_resource.meta.client.exceptions.NoSuchKey as e:
        logging.error(e)
        raise e


def create_presigned_url(bucket_name, object_name, expiration=432000):
    """Generate a presigned URL to share an S3 object
    https://boto3.amazonaws.com/v1/documentation/api/latest/guide/s3-presigned-urls.html#presigned-urls

    :param bucket_name: string
    :param object_name: string
    :param expiration: Time in seconds for the presigned URL to remain valid (5 days default)
    :return: Presigned URL as string. If error, returns None.
    """
    # Generate a presigned URL for the S3 object and return string (or None if failed).
    try:
        logging.info("Creating presigned URL with 5 day expiration.")
        response = s3_client.generate_presigned_url("get_object",
                                                    Params={"Bucket": bucket_name,
                                                            "Key": object_name},
                                                    ExpiresIn=expiration)
    except ClientError as e:
        logging.error(e)
        return None
    return response


def ses_email(presigned_url):
    """ Send email through Amazon SES using an AWS SDK. """
    ses_client = boto3.client("ses", region_name=REGION)
    # This address must be verified with Amazon SES.
    SENDER = "Sensor Data Uploader <DFO.PACScienceMSEAData-DonneesEASMSciencePAC.MPO@dfo-mpo.gc.ca>"
    # The subject line for the email.
    SUBJECT = "Processed sensor data link"
    # The email body for recipients with non-HTML email clients.
    BODY_TEXT = ("""You're receieving this email because you are a user of the 
                 Sensor Data Uploader (https://www.sensor-data-uploader.com/) application.
                 The link provided below is to download a zip file with one or more processed (flagged) csv and netCDF files (with embedded metadata). The link will expire in 5 days.
                 If this email has been sent to you in error, please contact the MSEA Data Stewardship Unit DFO.PACScienceMSEAData-DonneesEASMSciencePAC.MPO@dfo-mpo.gc.ca.
                 Processed File Download: {}.""".format(presigned_url)
                )   
    # The HTML body of the email.
    BODY_HTML = """<html>
    <head></head>
    <body>
    <h3>Sensor data processed data</h3>
    <p>You're receieving this email because you are a user of the <a href='https://www.sensor-data-uploader.com/'>Sensor Data Uploader</a>
    application. The link provided below is to download a zip file file with one or more processed (flagged) csv and netCDF files (with embedded metadata). The link will expire in 5 days.
    If this email has been sent to you in error, please contact the MSEA Data Stewardship Unit DFO.PACScienceMSEAData-DonneesEASMSciencePAC.MPO@dfo-mpo.gc.ca.
    </p>
    <p><a href='{}'>Processed File Download</a></p>
    </body>
    </html>
                """.format(presigned_url)
    # The character encoding for the email.
    CHARSET = "UTF-8"
    # Try to send the email.
    try:
        logging.info("Processing email...")
        #Provide the contents of the email.
        response = ses_client.send_email(
            Destination={
                'ToAddresses': RECIPIENT_LIST,
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,
            # If you are not using a configuration set, comment or delete the
            # following line
            # ConfigurationSetName=CONFIGURATION_SET,
        )
    # Display an error if something goes wrong.	
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])


def lambda_handler(event, context):
    logging.info("Received event: " + json.dumps(event, indent=2))
    bucket = get_bucket(event)
    key = get_key(event)
    presigned_url = create_presigned_url(bucket, key)
    ses_email(presigned_url)
